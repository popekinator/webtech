import React, {Component} from 'react';

class UserPannel extends Component{

    deleteAccount = (e) =>{
        e.preventDefault();
        let xhr;
        xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://transportexperiences.firebaseio.com/users/.json');
        xhr.onload = function() {
            let users
            if (xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                users = data;
                for(let i = 0; i < users.length; i++){
                    if(users[i].username ===  sessionStorage.getItem("User")){
                        let deleteAccount = window.confirm("Are you sure that you want to delete your account?");
                        if(deleteAccount){
                            let xhrUpdatePassword = new XMLHttpRequest();
                            xhrUpdatePassword.open('DELETE', `https://transportexperiences.firebaseio.com/users/${i}/.json`);
                            xhrUpdatePassword.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            xhrUpdatePassword.onload = function() {
                                if (xhr.status === 200) {
                                        sessionStorage.removeItem("User");
                                        sessionStorage.removeItem("Email");
                                        setTimeout(() => {  window.location.reload(); }, 2000);
                                    
                                }
                                else if (xhr.status !== 200) {
                                    alert('Request failed.  Returned status of ' + xhr.status);
                                }
                            };
                            xhrUpdatePassword.send();
                        }
                    }
                }

                
            }
            else if (xhr.status !== 200) {
                alert('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();
    }

    updatePassword = (e) =>{
        e.preventDefault();
    let resetArea       = document.getElementById("showPannel");
    let answerReset     = document.getElementById("securityCheck");
    let passwordReset   = document.getElementById("password1");

    let users;
    let xhr;
    xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://transportexperiences.firebaseio.com/users/.json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            let data = JSON.parse(xhr.responseText);
            users = data;
            
            for(let i = 0; i < users.length; i++){
                if(users[i].username ===  sessionStorage.getItem("User")){
                    if(users[i].checkRule === answerReset.value){
                        let xhrUpdatePassword = new XMLHttpRequest();
                        xhrUpdatePassword.open('PUT', `https://transportexperiences.firebaseio.com/users/${i}/password/.json`);
                        xhrUpdatePassword.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                        xhrUpdatePassword.onload = function() {
                            if (xhr.status === 200) {
                                resetArea.classList.add("hide");
                                setTimeout(() => {  window.location.reload(); }, 2000);
                            }
                            else if (xhr.status !== 200) {
                                alert('Request failed.  Returned status of ' + xhr.status);
                            }
                        };
                        xhrUpdatePassword.send(passwordReset.value);
                    }else{
                        alert("Your check rule is WRONG, try again!");
                    }
                }
            }
        }
        else if (xhr.status !== 200) {
            alert('Request failed.  Returned status of ' + xhr.status);
        }
    };
    xhr.send();
    }

    render(){
        const user  = sessionStorage.getItem("User");
        const email = sessionStorage.getItem("Email");
        return(
            <div className="row my-5 hide" id="showPannel">
                <div className="col-12 py-2">Manage Your Account Settings</div>
                <div className="col-12 py-2">
                    <form>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Enter your @username</label>
                            <input type="text" className="form-control" id="pannelUser" value={user} readOnly />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Enter your @email</label>
                            <input type="text" className="form-control" id="pannelEmail" aria-describedby="userHelp" value={email} readOnly/>
                            <small id="userHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="passwordFirst">Enter your @password</label>
                            <input type="password" className="form-control" id="password1"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Enter your @password again</label>
                            <input type="password" className="form-control" id="password2"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Enter your @security check</label>
                            <input type="text" className="form-control" id="securityCheck" aria-describedby="userHelp"/>
                            <small id="securityCheckInfo" className="form-text text-muted">Please note, this will be used to reset password.</small>
                        </div>
                        <button onClick={this.updatePassword} type="submit" className="btn btn-primary">Save Settings</button>
                        <button onClick={this.deleteAccount} type="submit" className="btn btn-danger ml-2">Delete my account</button>
                    </form>
                </div>
            </div> 
        );
    }
}

export default UserPannel;